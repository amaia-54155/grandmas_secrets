// Fetch the recipe details
async function fetchRecipe(id) {
    let recipeTitle = document.getElementById("recipe-title");
    let recipeImg = document.getElementById("recipe-img");
    let recipeInfo = document.getElementById("recipe-info");
    let ingredientsList = document.getElementById("recipe-ingredients");

    const response = await fetch(
        `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${id}`
    );
    const data = await response.json();
    const recipe = data.meals[0];

    // Populate the recipe details
    recipeTitle.textContent = recipe.strMeal;
    recipeImg.src = recipe.strMealThumb;
    recipeImg.alt = recipe.strMeal;
    let p = recipeInfo.getElementsByTagName("p");
    p[0].innerText = recipe.strInstructions;

    let videourl = recipe.strYoutube;
    if (videourl != "") {
        var id = videourl.split("?v=")[1];
        var embedlink = "http://www.youtube.com/embed/" + id;

        let iframe = document.createElement("iframe");
        iframe.setAttribute("src", embedlink);
        iframe.setAttribute("width", 270);
        document.getElementById("iframecontainer").appendChild(iframe);
    }

    // Populate the ingredients list
    for (let i = 1; i <= 20; i++) {
        if (recipe[`strIngredient${i}`]) {
            const ingredient = document.createElement("li");
            ingredient.textContent = `${recipe[`strIngredient${i}`]} - ${recipe[`strMeasure${i}`]
                }`;
            ingredientsList.appendChild(ingredient);
        }
    }
}

// Show the recipe details
function showRecipeDetails(id) {
    fetchRecipe(id);
    recipeDetails.classList.remove("hideme");
}

// Hide the recipe details
function hideRecipeDetails() {
    recipeDetails.classList.add("hideme");
}

function toggleDetailInfo() {
    let toggleBtn = document.querySelector(".toggle-btn");
    if (toggleBtn.innerHTML == "Instructions") {
        toggleBtn.innerHTML = "Ingredients";
        document.getElementById("details-title").innerHTML = "Instructions:";
        document.getElementById("recipe-ingredients").classList.add("hideme");
        document.getElementById("recipe-instructions").classList.remove("hideme");
        document.querySelector("#recipe-info").style.width = "auto";
    } else {
        toggleBtn.innerHTML = "Instructions";
        document.getElementById("details-title").innerHTML = "Ingredients:";
        document.getElementById("recipe-ingredients").classList.remove("hideme");
        document.getElementById("recipe-instructions").classList.add("hideme");
        document.querySelector("#recipe-info").style.width = "100%";
    }
}