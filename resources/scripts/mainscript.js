// EVENT LISTENERS 
window.onload = function () {
  // Handle navigation bar items click
  document.querySelectorAll(".nav-item").forEach(function (navItem) {
    navItem.addEventListener("click", navClicked);
  });

  // Handle "My Recipes" tab click to load the favorite recipes
  document
    .querySelector("#myrecipes-tab")
    .addEventListener("click", getFavRecipes);

  // Handle favorite star click
  document.querySelector("#myrecipes").addEventListener("click", (e) => {
    addToFavorites(e);
  });

  // Handle click on map filter
  document.querySelector("#mapfilter").addEventListener("click", (e) => {
    toggleFavButton("not def");
  });

  // Handle search bar input 
  document.querySelector("#searchBar").addEventListener("keyup", (e) => {
    searchCategory(e);
  });

  // Handle click on recipe cards
  document.querySelector(".container").addEventListener("click", (e) => {
    handleCardClicks(e);
  });

  // Handle click on landing page (recicpe of the day)
  document.querySelector("#rotd").addEventListener("click", (e) => {
    if (e.target.classList.contains("favicon")) {
      switchFavIcon(e);
      addRecipeToFavorites(e.target.getAttribute("id"));
    } else if (e.target.classList.contains("detailsBtn")) {
      handleCardClicks(e);
    }
  });

  // Handle the category dropdown
  document.querySelector(".dropdown-content").addEventListener("click", (e) => {
    const category = e.target.textContent;
    showHomeTab();
    fetchRecipes("c", category);
  });

  // Handle the details back button
  document
    .querySelector(".back-btn")
    .addEventListener("click", (e) => {
      hideRecipeDetails();
      document.getElementById("iframecontainer").innerHTML = "";
    });

  // Handle the details favorite button toggle
  document
    .querySelector(".toggle-btn")
    .addEventListener("click", toggleDetailInfo);

  // Handle the search bar result list click
  document.getElementById("resultList").addEventListener("click", (e) => {
    const target = e.target;
    document.getElementById("resultList").style.display = "none";
    document.getElementById("searchBar").value = target.textContent;
    let filter = "";
    showHomeTab();

    if (target.classList.contains("cat")) {
      filter = "c";
    } else if (target.classList.contains("ing")) {
      filter = "i";
    }
    fetchRecipes(filter, target.textContent);
  });
};

//Navigation tab click listener
function navClicked(event) {
  let selected_nav = event.currentTarget.id; // Get the ID of the selected element.
  let selected_article = selected_nav.replace("-tab", "");

  document.querySelectorAll(".tab-content").forEach(function (article) {
    if (article.id === selected_article) {
      article.classList.remove("hideme");
    } else {
      article.classList.add("hideme");
    }
  });
}

//Show home tab
function showHomeTab() {
  document.querySelectorAll(".tab-content").forEach(function (elem) {
    elem.classList.add("hideme");
    if (elem.getAttribute("id") == "home") {
      elem.classList.remove("hideme");
    }
  });
}

function switchFavIcon(e) {
  if (e.target.src.includes("star.png")) {
    e.target.src = "resources/images/favorite.png";
  } else {
    e.target.src = "resources/images/star.png";
  }
}

function createCard(recipe, boolfab = false) {
  let imagen = "favorite.png";
  if (boolfab) imagen = "star.png";

  let cardInnerHTML = `
    <div class="cardcontainer">
    <img src="${recipe.strMealThumb}" alt="${recipe.strMeal}" />
        <button class="favbutton">
                <img class="favicon" id="${recipe.idMeal}" src="resources/images/${imagen}">    
            </button>
    </div>
    <p>${recipe.strMeal}</p>
    <div class="cardbuttons">
        <a href="#" class="detailsBtn" data-id="${recipe.idMeal}">View Details</a>
    </div>`;
  return cardInnerHTML;
}

function searchCategory(e) {
  let input, filter, list;
  input = document.getElementById("searchBar");
  filter = input.value.toUpperCase();
  list = document.getElementById("resultList");
  li = list.getElementsByTagName("a");
  list.style.display = "block";

  for (let i = 0; i < li.length; i++) {
    txtValue = li[i].textContent || li[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      li[i].style.display = "";
    } else {
      li[i].style.display = "none";
    }
    if (filter == "") {
      list.style.display = "none";
    }
  }
  if (e.key === "Enter") {
    list.style.display = "none";
    searchFree();
  }
}

async function searchFree() {
  let search = document.querySelector("#searchBar").value;
  const resp = await fetch(
    `https://www.themealdb.com/api/json/v1/1/search.php?s=${search}`
  );
  const data = await resp.json();
  let recipes = data.meals;

  showHomeTab();
  container.innerHTML = "";

  // Create cards for each recipe
  let favList = getFavoriteRecipesList();
  if (recipes != null) {
    recipes.forEach((recipe) => {
      const card = document.createElement("div");
      card.classList.add("card");
      card.innerHTML = createCard(recipe, favList.includes(recipe.idMeal));
      container.appendChild(card);
    });
  } else {
    const msg = "<div class='center'><p>No results found.</p><div>";
    container.innerHTML = msg;
  }
}

// Fetch the recipes by category
async function fetchRecipes(by = "c", input) {
  const response = await fetch(
    `https://www.themealdb.com/api/json/v1/1/filter.php?${by}=${input}`
  );
  const data = await response.json();
  const recipes = data.meals;

  // Clear the existing cards
  container.innerHTML = "";
  // Create cards for each recipe
  let favList = getFavoriteRecipesList();

  recipes.forEach((recipe) => {
    const card = document.createElement("div");
    card.classList.add("card");
    card.innerHTML = createCard(recipe, favList.includes(recipe.idMeal));
    container.appendChild(card);
  });
}

async function fetchRandomRecipe() {
  const recipeOfTheDay = document.querySelector("#rotd");
  const response = await fetch(
    "https://www.themealdb.com/api/json/v1/1/random.php"
  );
  const data = await response.json();
  const recipe = data.meals[0];

  let bigCardHTML = `
     <h1 class="subtitle">Get inspired!</h1>
      <div class="big">
      <div>
        <button class="homefavbtn">
            <img class="favicon" id="${recipe.idMeal}" src="resources/images/favorite.png">    
        </button>
        <img class="mealimg" src="${recipe.strMealThumb}" alt="${recipe.strMeal}" />
      </div>
        <div class="cardbuttons">
     <a class="btn" href="fullindex.html">Next Recipe</a>

            <h3>${recipe.strMeal}</h3>
            <p>Area: ${recipe.strArea}</p>
            <p>Category: ${recipe.strCategory}</p>
            <a href="#" class="detailsBtn" data-id="${recipe.idMeal}">View Details</a>
        </div>
      </div>
      `;
  recipeOfTheDay.innerHTML = bigCardHTML;
}

// Fetch the categories
async function fetchCategories() {
  const categoriesDropdown = document.querySelector(".dropdown-content");

  const response = await fetch(
    "https://www.themealdb.com/api/json/v1/1/list.php?c=list"
  );
  const data = await response.json();
  const categories = data.meals;

  const resp = await fetch(
    "https://www.themealdb.com/api/json/v1/1/list.php?i=list"
  );
  const ingredientdata = await resp.json();
  const ingredients = ingredientdata.meals;

  // Populate the categories tab
  categories.forEach((category) => {
    categoriesDropdown.innerHTML += `<a href="#">${category.strCategory}</a>`;
    resultList.innerHTML += `<a class="cat">${category.strCategory}</a>`;
  });
  ingredients.forEach((ingredient) => {
    resultList.innerHTML += `<a class="ing">${ingredient.strIngredient}</a>`;
  });
}

function handleCardClicks(e) {
  if (e.target.classList.contains("detailsBtn")) {
    showRecipeDetails(e.target.getAttribute("data-id"));
  } else if (e.target.classList.contains("favicon")) {
    switchFavIcon(e);
    let add = e.target.getAttribute("src").includes("star");
    addRecipeToFavorites(e.target.getAttribute("id"));
  }
}