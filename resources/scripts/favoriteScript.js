let favRecipes = JSON.parse(localStorage.getItem("favRecipes")) || [];
let favDict = {};

function addRecipeToFavorites(recipeId) {
  if (typeof Storage !== "undefined") {
    if (favRecipes.includes(recipeId)) {
      favRecipes.splice(favRecipes.indexOf(recipeId), 1);
    } else {
      favRecipes.push(recipeId);
    }
    localStorage.setItem("favRecipes", JSON.stringify(favRecipes));
  } else {
    alert("Sorry, your browser does not support Web Storage...");
  }
}

function getFavRecipes() {
  let favContainer = document.querySelector("#myrecipes");

  favRecipes = getFavoriteRecipesList();
  if (favRecipes.length == 0) {
    favContainer.innerHTML =
      "<div class='center'><p>You have no favorite recipes yet.</p><div>";
  } else {
    // Clear the existing cards
    favContainer.innerHTML = "";

    favRecipes.forEach(async (element) => {
      const resp = await fetch(
        `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${element}`
      );
      const data = await resp.json();
      let recipe = data.meals[0];
      const card = document.createElement("div");
      card.classList.add("card");
      card.innerHTML = createCard(recipe, true);

      favContainer.appendChild(card);
    });
    favContainer.addEventListener("click", (e) => {
      if (e.target.classList.contains("detailsBtn")) {
        showRecipeDetails(e.target.getAttribute("data-id"));
      }
    });
  }
}

function getFavoriteRecipesList() {
  let favRecipes = [];
  if (typeof Storage !== "undefined") {
    if (localStorage.getItem("favRecipes") != null) {
      favRecipes = JSON.parse(localStorage.getItem("favRecipes"));
    }
    return favRecipes;
  } else {
    alert("Sorry, your browser does not support Web Storage...");
  }
}

async function getFavoritesDict() {
  const resp = await fetch("./resources/jsonfiles/data.json");
  const regionData = await resp.json();
  favDict = {};
  favRecipes = getFavoriteRecipesList();
  if (favRecipes.length != 0) {
    favRecipes.forEach(async (element) => {
      fetch(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${element}`, {
        mode: "cors",
      })
        .then((resp) => resp.json())
        .then((data) => {
          let recipe = data.meals[0];
          var region = regionData.filter(function (e) {
            return e.demonym == recipe.strArea;
          });
          var regionName = region[0].name;

          if (regionName in favDict) {
            favDict[regionName] += 1;
          } else {
            favDict[regionName] = 1;
          }
        })
        .then((fin) => {
          drawFavoritesMap(favDict);
        });
    });
  }
}

function addToFavorites(e) {
  if (e.target.classList.contains("favicon")) {
    switchFavIcon(e);
    addRecipeToFavorites(e.target.getAttribute("id"));
    getFavRecipes();
  }
}


