var chartGeneral;
var chartFav;
let dataDict = {};
let areasList = [];

async function getAreas() {
  const resp = await fetch("./resources/jsonfiles/data.json");
  const regionData = await resp.json();

  fetch(`https://www.themealdb.com/api/json/v1/1/list.php?a=list`, {
    mode: "cors",
  })
    .then((response) => response.json())
    .then((data) => {
      data.meals.forEach((element) => {
        var area = element.strArea;

        if (area != "Unknown") {
          var region = regionData.filter(function (e) {
            return e.demonym == area;
          });
          dataDict[area] = region[0].name;
          areasList.push(area);
        }
      });
    })
    .then((fin) => {
      google.charts.setOnLoadCallback(drawRegionsMap(dataDict));
    });
}

async function drawRegionsMap(dict) {
  var data = new google.visualization.DataTable();
  data.addColumn("string", "Country");
  data.addColumn("number", "How many recipes");

  for (let i in dict) {
    data.addRows([[dict[i], await getMealsQuantity(i)]]);
  }

  var options = {
    height: 500,
    minValue: 0,
    colors: ["#e9daf7", "#9123f7"],
  };

  chartGeneral = new google.visualization.GeoChart(
    document.getElementById("geochart-colors")
  );
  google.visualization.events.addListener(
    chartGeneral,
    "select",
    selectHandler
  );
  chartGeneral.draw(data, options);
}

function selectHandler() {
  var selectedItem = chartGeneral.getSelection()[0];
  var value = areasList[selectedItem.row];
  showHomeTab();
  fetchRecipes("a", value);
}

async function getMealsQuantity(area) {
  const resp = await fetch(
    `https://www.themealdb.com/api/json/v1/1/filter.php?a=${area}`
  );
  const data = await resp.json();
  var count = Object.keys(data.meals).length;
  return count;
}

function drawFavoritesMap(favDict) {
  var data = new google.visualization.DataTable();
  data.addColumn("string", "Country");
  data.addColumn("number", "How many recipes");

  for (let i in favDict) {
    data.addRows([[i, favDict[i]]]);
  }

  var options = {
    height: 500,
    minValue: 0,
    colors: ["#ADD8E6", "#00008B"],
  };

  chartFav = new google.visualization.GeoChart(
    document.getElementById("geochart-fav")
  );
  google.visualization.events.addListener(chartFav, "select", favSelectHandler);
  chartFav.draw(data, options);
}

function favSelectHandler() {
  var selectedItem = chartFav.getSelection()[0];
  let reg = Object.keys(favDict)[selectedItem.row];
  let deno = Object.keys(dataDict).find((key) => dataDict[key] === reg);
  showHomeTab();
  fetchRecipes("a", deno);
}

function toggleFavButton(set = "default") {
  let filterBtn = document.getElementById("filterTxt");
  if (filterBtn.innerHTML == "General" || set == "default") {
    filterBtn.innerHTML = "Favorite";
    document.getElementById("geochart-colors").classList.remove("hideme");
    document.getElementById("geochart-fav").classList.add("hideme");
  } else {
    filterBtn.innerHTML = "General";
    document.getElementById("geochart-colors").classList.add("hideme");
    document.getElementById("geochart-fav").classList.remove("hideme");
    if (!document.getElementById("geochart-fav").hasChildNodes()) {
      document.getElementById("geochart-fav").innerHTML = `<div class='center'>
                <p>Start adding recipes to your favorites.</p>
            <div>`;
    }
    google.charts.setOnLoadCallback(getFavoritesDict());
  }
}
